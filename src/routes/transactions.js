const express = require("express");
const getAccessToken = require("../middlewares/getAccessToken/getAccessToken");
const retrieveTransactions = require("../middlewares/retrieveTransactions/retrieveTransactions");

const router = express.Router();

router.get("/:userId/transactions", getAccessToken, retrieveTransactions);
module.exports = router;
