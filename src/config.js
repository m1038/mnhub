const apiUrl = process.env.API_URL || "https://obmockaspsp.moneyhub.co.uk/api";

module.exports = apiUrl;
