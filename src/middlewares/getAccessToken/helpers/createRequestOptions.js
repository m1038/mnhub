const createAuthString = (accessToken) => {
  return `Basic ${accessToken}`;
};

const createRequestOptions = (apiURL, accessToken, data) => {
  return {
    method: "POST",
    url: `${apiURL}/token`,
    headers: {
      Authorization: createAuthString(accessToken),
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: data,
  };
};

module.exports = createRequestOptions;
