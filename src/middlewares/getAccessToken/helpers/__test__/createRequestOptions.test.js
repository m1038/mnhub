const createRequestOptions = require("../createRequestOptions");

test("if createRequestOptions module returns the correct options object", async () => {
  const options = createRequestOptions(
    "http://url.com",
    "YmExYmRjdszAtNjBmNS00OTM5dsFmYzQtMWIxM2E5OGRjdswOjZmMWFmZmY4LWViODEtNDk0NS24YjkxLWEwNWUzZDA5NWNlMw",
    "data"
  );
  expect(options).toStrictEqual({
    method: "POST",
    url: "http://url.com/token",
    headers: {
      Authorization:
        "Basic YmExYmRjdszAtNjBmNS00OTM5dsFmYzQtMWIxM2E5OGRjdswOjZmMWFmZmY4LWViODEtNDk0NS24YjkxLWEwNWUzZDA5NWNlMw",
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: "data",
  });
});
