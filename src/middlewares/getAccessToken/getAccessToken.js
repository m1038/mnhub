const axios = require("axios");
var qs = require("qs");
const apiURL = require("../../config");

const createRequestOptions = require("./helpers/createRequestOptions");

const getAccessToken = async (req, res, next) => {
  const data = qs.stringify({
    client_id: "ba1bdcc0-60f5-4939-afc4-1b13a98dc490",
    client_secret: "6f1afff8-eb81-4945-8b91-a05e3d095ce3",
    grant_type: "client_credentials",
    scope: "transactions",
  });

  try {
    const response = await axios.request(
      createRequestOptions(
        apiURL,
        "YmExYmRjYzAtNjBmNS00OTM5LWFmYzQtMWIxM2E5OGRjNDkwOjZmMWFmZmY4LWViODEtNDk0NS04YjkxLWEwNWUzZDA5NWNlMw",
        data
      )
    );
    res.locals.accessToken = response.data.access_token;
    next();
  } catch (err) {
    res.status(403).send({ message: "Invalid token" });
    console.log("server error: ", error);
  }
};

module.exports = getAccessToken;
