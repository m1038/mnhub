const axios = require("axios");
const formatTransactions = require("./helpers/formatTransactions");
const createRequestOptions = require("./helpers/createRequestOptions");
const apiURL = require("../../config");

const retrieveTransactions = async (req, res) => {
  try {
    const options = createRequestOptions(
      apiURL,
      req.params.userId,
      res.locals.accessToken
    );

    const response = await axios.request(options);

    res.status(200).send(formatTransactions(response.data));
  } catch (error) {
    res
      .status(401)
      .send({ message: "User does not have access to the transactions" });
    console.log("server error: ", error);
  }
};

module.exports = retrieveTransactions;
