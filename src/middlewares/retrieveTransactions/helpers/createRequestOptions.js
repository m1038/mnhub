const createAuthString = (accessToken) => {
  return `Bearer ${accessToken}`;
};

const createURL = (apiURL, userId) => {
  return `${apiURL}/users/${userId}/transactions`;
};

const createRequestOptions = (apiURL, userId, accessToken) => {
  return {
    method: "GET",
    url: createURL(apiURL, userId),
    headers: {
      Authorization: createAuthString(accessToken),
    },
  };
};

module.exports = createRequestOptions;
