const formatTransactions = require("../formatTransactions");

test("if formatTransactions module formats the transactions objects in an array correctly", async () => {
  const transactions = formatTransactions({
    Data: {
      Transactions: [
        {
          AccountId: "33",
          BookingDateTime: "2020-01-01T00:00:00.000Z",
          ValueDateTime: "2020-01-02T00:00:00.000Z",
          TransactionInformation: "DOLORE SIT LOREM",
          CreditDebitIndicator: "Credit",
          Amount: {
            Amount: 19.67,
            Currency: "GBP",
          },
          TransactionId: "da9f3be7-0a32-4c59-922e-a744a558860e",
          Status: "Pending",
        },
      ],
    },
  });
  expect(transactions).toStrictEqual({
    data: [
      {
        accountId: "33",
        amount: 19.67,
        date: "2020-01-02T00:00:00.000Z",
        description: "Dolore sit lorem",
        id: "da9f3be7-0a32-4c59-922e-a744a558860e",
        status: "pending",
      },
    ],
  });
});
