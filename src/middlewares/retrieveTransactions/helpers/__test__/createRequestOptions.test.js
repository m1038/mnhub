const createRequestOptions = require("../createRequestOptions");

test("if createRequestOptions module returns the correct options object", async () => {
  const options = createRequestOptions(
    "http://url.com",
    "133",
    "YmExYmRjdszAtNjBmNS00OTM5dsFmYzQtMWIxM2E5OGRjdswOjZmMWFmZmY4LWViODEtNDk0NS24YjkxLWEwNWUzZDA5NWNlMw"
  );
  expect(options).toStrictEqual({
    method: "GET",
    url: "http://url.com/users/133/transactions",
    headers: {
      Authorization:
        "Bearer YmExYmRjdszAtNjBmNS00OTM5dsFmYzQtMWIxM2E5OGRjdswOjZmMWFmZmY4LWViODEtNDk0NS24YjkxLWEwNWUzZDA5NWNlMw",
    },
  });
});
