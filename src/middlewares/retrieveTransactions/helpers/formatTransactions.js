const capitalizeFirstLetter = (s) =>
  (s && s[0].toUpperCase() + s.slice(1)) || "";

const formatTransactionItem = (transaction) => {
  const newTransaction = {
    id: transaction.TransactionId,
    accountId: transaction.AccountId,
    amount: transaction.Amount.Amount,
    date: transaction.ValueDateTime,
    description: capitalizeFirstLetter(
      transaction.TransactionInformation.toLowerCase()
    ),
    status: transaction.Status.toLowerCase(),
  };
  return newTransaction;
};

const formatTransactionsArr = (transactions) => {
  const newTransactions = [];
  for (const element of transactions) {
    newTransactions.push(formatTransactionItem(element));
  }
  return newTransactions;
};

const formatTransactions = (data) => {
  return { data: formatTransactionsArr(data.Data.Transactions) };
};

module.exports = formatTransactions;
