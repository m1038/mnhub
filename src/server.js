const express = require("express");
const morgan = require("morgan");
require('dotenv').config()

const app = express();

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");

const transactionsRouter = require("./routes/transactions");
app.use(morgan('dev'));

app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/api/users", transactionsRouter);

module.exports = app;
